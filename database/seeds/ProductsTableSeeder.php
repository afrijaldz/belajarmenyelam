<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into products(name, price, stock) values :n, :p, :s)', [
          'n'=>'Brio Sport E',
          'p'=>'180000000',
          's'=>'14'
        ]);
        DB::insert('insert into products (name, price, stock) values(:n, :p, :s)',[
          'n'=>'Brio Satya E',
          'p'=>'125900000',
          's'=>'23'
        ]);
        $this->command->info('Berhasil menambah 2 mobil');

    }
}
