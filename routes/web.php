<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('kontak', function(){
  return '<h1>halaman kontak</h1>';
});

Route::post('kontak-post', function(){
return 'Anda mengirim pesan "' . $_POST['pesan'] . '"';
});

Route::any('kontak', function(){
  $html = '<h1>halaman kontak</h1>';
  $html .= 'Anda mengakses dengan method ' . Request::method();
if (Request::isMethod('delete')) {
  $html .= "<br>Anda telah menghapus data</br>";
}
  return $html;
});

Route::group(['prefix'=>'dashboard'], function(){
  Route::get('settings', function(){
    return 'halaman dashboard > settings';
  });
  Route::get('profile', function(){
    return 'halaman dashboard > profile';
  });
  Route::get('inbox', function(){
    return 'halaman dashboard > inbox';
  });
});

Route::group(['domain'=>'{username}.dzuhri.net'], function(){
  Route::get('/', function($username){
    return 'Anda mengunjungi akun ' . $username;
  });
});

Route::get('menu', function(){
  return 'kunjungi <a href="'.route('welcome.yo').'">Halaman Welcome</a>';
});

Route::get('welcome/{nama?}', ['as'=>'welcome.yo', function($nama='afrijal'){
  return "Selamat datang " . $nama . ". oke";
}]);
